<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactMassagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact__massages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Sender_Name');
            $table->string('Sender_Email');
            $table->longText('Sender_Message');
            $table->timestamps();
            $table->integer('status')->default(1);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact__massages');
    }
}
