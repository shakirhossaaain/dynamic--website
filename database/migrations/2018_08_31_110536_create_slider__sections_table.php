<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider__sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('under_slider_picture');
            $table->longText('Slider_1st_Text_Header');
            $table->longText('Slider_1st_Text');
            $table->longText('Slider_2nd_Text_Header');
            $table->longText('Slider_2nd_Text');
            $table->longText('Slider_3rd_Text_Header');
            $table->longText('Slider_3rd_Text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider__sections');
    }
}
