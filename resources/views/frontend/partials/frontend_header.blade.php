<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Negotiation Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{asset('main_web_template/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset('main_web_template/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- Styles -->

<!-- js -->
<script type="text/javascript" src="{{asset('main_web_template/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('main_web_template/js/main.js')}}"></script>

<!-- //js -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{asset('main_web_template/css/font-awesome.min.css')}}" />
<!-- //font-awesome icons -->
<link rel="stylesheet" href="{{asset('main_web_template/css/flexslider.css')}}" type="text/css" media="screen" property="" />
<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet">
 </head>
