<body>
<!-- banner -->
	<div class="w3ls-banner-info-bottom">
		<div class="container">
			<div class="banner-address">
				<div class="col-md-3 banner-address-left">
					<p><i class="fa fa-map-marker" aria-hidden="true"></i> Broome St, NY 10002.</p>
				</div>
				<div class="col-md-3 banner-address-left">
					<p><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:example@email.com">mail@example.com</a></p>
				</div>
				<div class="col-md-3 banner-address-left">
					<p><i class="fa fa-phone" aria-hidden="true"></i> +1 234 567 8901</p>
				</div>
					<div class="col-md-3 agile_banner_social">
						<ul class="agileits_social_list">
							<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
							<li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="header">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<h1><a class="navbar-brand" href="{{route('home')}}">Negotiation</a></h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="cl-effect-13" id="cl-effect-13">
						<ul class="nav navbar-nav">
							<li class="active"><a href="{{route('home')}}">Home</a></li>
							<li><a href="{{route('services')}}">Services</a></li>
							<li><a href="{{route('portfolio')}}">Portfolio</a></li>
							<li><a href="{{route('contact')}}">Contact</a></li>
						</ul>
						<div class="w3_agile_login">
							<div class="cd-main-header">
								<a class="cd-search-trigger" href="#cd-search"> <span></span></a>
								<!-- cd-header-buttons -->
							</div>
							<div id="cd-search" class="cd-search">
								<form action="#" method="post">
									<input name="Search" type="search" placeholder="Search...">
								</form>
							</div>
						</div>
					</nav>
				</div>
			</nav>
		</div>
	</div>
	<!-- //banner -->
