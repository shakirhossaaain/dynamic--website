@extends('frontend.layout.frontend_layout')

@section('title', 'Home')

@section('content')

<div style="background: url({{asset('storage').'/'.$Slider_Section_all->under_slider_picture}}) no-repeat center fixed" class="banner">
  <div class="container">
    <section class="slider">
        <div class="flexslider">
          <ul class="slides">
            <li>
              <div class="agileits_w3layouts_banner_info">
                <h3>{{ $Slider_Section_all -> Slider_1st_Text_Header}}</h3>
                <p>{{ $Slider_Section_all -> Slider_1st_Text}}</p>
                <div class="agileits_w3layouts_more">
                  <a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
                </div>
              </div>
            </li>
            <li>
              <div class="agileits_w3layouts_banner_info">
                <h3>{{ $Slider_Section_all -> Slider_2nd_Text_Header}}</h3>
                <p>{{ $Slider_Section_all -> Slider_2nd_Text}}</p>
                <div class="agileits_w3layouts_more">
                  <a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
                </div>
              </div>
            </li>
            <li>
              <div class="agileits_w3layouts_banner_info">
                <h3>simply dummy text of the printing</h3>
                <p>Standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                <div class="agileits_w3layouts_more">
                  <a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </section>
    <!-- flexSlider -->
      <script defer src="{{asset('main_web_template/js/jquery.flexslider.js')}}"></script>
      <script type="text/javascript">
        $(window).load(function(){
          $('.flexslider').flexslider({
          animation: "slide",
          start: function(slider){
            $('body').removeClass('loading');
          }
          });
        });
      </script>
    <!-- //flexSlider -->
  </div>
</div>
<!-- //banner -->
<!-- services -->
<div class="services">
  <div class="container">
  <h2 class="w3ls_head"><span>Wel </span>come</h2>
    <div class="services-w3lsrow">
      <div class="col-md-4 services-grids top">
        <i class="fa fa-database" aria-hidden="true"></i>
        <h4>Fugiat Quo</h4>
        <p>To ensure the quality and effectiveness of every business activity, we support our every client by supporting them in all legislative issues.</p>
      </div>
      <div class="col-md-4 services-grids top-1">
        <i class="fa fa-group icon" aria-hidden="true"></i>
        <h4>Voluptas </h4>
        <p>To ensure the quality and effectiveness of every business activity, we support our every client by supporting them in all legislative issues.</p>
      </div>
      <div class="col-md-4 services-grids top-2">
        <i class="fa fa-cubes" aria-hidden="true"></i>
        <h4>Quo fugiat</h4>
        <p>To ensure the quality and effectiveness of every business activity, we support our every client by supporting them in all legislative issues.</p>
      </div>
      <div class="col-md-4 services-grids top-3">
        <i class="fa fa-gears icon" aria-hidden="true"></i>
        <h4>Voluptas</h4>
        <p>To ensure the quality and effectiveness of every business activity, we support our every client by supporting them in all legislative issues.</p>
      </div>
      <div class="col-md-4 services-grids top-4">
        <i class="fa fa-external-link" aria-hidden="true"></i>
        <h4>Quo fugiat </h4>
        <p>To ensure the quality and effectiveness of every business activity, we support our every client by supporting them in all legislative issues.</p>
      </div>
      <div class="col-md-4 services-grids top-5">
        <i class="fa fa-briefcase icon" aria-hidden="true"></i>
        <h4>Fugiat Quo</h4>
        <p>To ensure the quality and effectiveness of every business activity, we support our every client by supporting them in all legislative issues.</p>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>
<!-- //services -->
<!-- stats -->
<div class="sta-agile">
  <div class="stat-agile-info">
    <div class="container">
      <div class="stats">
        <div class="col-md-3 w3_counter_grid">
          <div class="w3_agileits_counter_grid">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <p class="counter">965</p>
          <h3>TRUSTED CLIENTS</h3>
        </div>
        <div class="col-md-3 w3_counter_grid">
          <div class="w3_agileits_counter_grid">
            <i class="fa fa-trophy" aria-hidden="true"></i>
          </div>
          <p class="counter">20</p>
          <h3>AWARDS</h3>
        </div>
        <div class="col-md-3 w3_counter_grid">
          <div class="w3_agileits_counter_grid">
            <i class="fa fa-asterisk" aria-hidden="true"></i>
          </div>
          <p class="counter">15</p>
          <h3>YEARS OF EXPERIENCE</h3>
        </div>
        <div class="col-md-3 w3_counter_grid">
          <div class="w3_agileits_counter_grid">
            <i class="fa fa-cog" aria-hidden="true"></i>
          </div>
          <p class="counter">24</p>
          <h3>EXPERTS</h3>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
</div>
<!-- //stats -->
<!-- stats -->
<script src="{{asset('main_web_template/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('main_web_template/js/jquery.countup.js')}}"></script>
  <script>
    $('.counter').countUp();
  </script>
<!-- //stats -->
<!-- about-team -->
<div class="team">
  <div class="container">
    <h2 class="w3ls_head"><span>Our </span>Team</h2>
    <div class="team-row-agileinfo">
      <div class="col-md-3 col-sm-6 col-xs-6 team-grids">
        <div class="thumbnail team-agileits">
          <img src="images/t1.jpg" class="img-responsive" alt=""/>
          <div class="w3agile-caption">
            <h4>Vaura Tegsner</h4>
            <p>Manager</p>
            <div class="social-w3lsicon">
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 team-grids">
        <div class="thumbnail team-agileits">
          <img src="images/t2.jpg" class="img-responsive" alt=""/>
          <div class="w3agile-caption">
            <h4>Jark Kohnson</h4>
            <p>Manager</p>
            <div class="social-w3lsicon">
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 team-grids">
        <div class="thumbnail team-agileits">
          <img src="images/t3.jpg" class="img-responsive" alt=""/>
          <div class="w3agile-caption">
            <h4>Chunk Erson</h4>
            <p>Manager</p>
            <div class="social-w3lsicon">
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 team-grids">
        <div class="thumbnail team-agileits">
          <img src="images/t4.jpg" class="img-responsive" alt=""/>
          <div class="w3agile-caption">
            <h4>Goes Mehak</h4>
            <p>Manager</p>
            <div class="social-w3lsicon">
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>
<!-- //about-team -->
<!-- testimonials -->
<div class="col-md-6 testimonials">
  <h3 class="w3ls_head"><span>Testim</span>onials</h3>
  <div class="testi-w3agileinfo">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#testi" aria-controls="testi" role="tab" data-toggle="tab"><img src="images/1.png" alt=""/></a></li>
      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><img src="images/3.png" alt=""/></a></li>
      <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><img src="images/2.png" alt=""/></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="testi">
        <h5>FILAN FISTEKU</h5>
        <p>Donec libero dui, scelerisque ac augue id, tristique ullamcorper elit. Nam ultrices, lacus vitae adipiscing aliquet.</p>
      </div>
      <div role="tabpanel" class="tab-pane" id="profile">
        <h5>ULLAMCORPER FILAN </h5>
        <p>Scelerisque ac augue id Donec libero dui, , tristique ullamcorper elit. Nam ultrices, lacus vitae adipiscing aliquet.</p>
      </div>
      <div role="tabpanel" class="tab-pane" id="messages">
        <h5>SCELERISQUE AUGUE</h5>
        <p>Nam ultrices lacus vitae adipiscing aliquet, metus ipsum imperdiet libero, vitae dignissim sapientristique Donec.</p>
      </div>
    </div>
  </div>
</div>
<div class="col-md-6 testimonials-left-w3">
</div>
<div class="clearfix"></div>
<!-- //testimonials -->
<!-- news-letter -->
<div class="news-letter">
  <div class="container">
    <h3 class="w3ls_head"><span>Subscribe </span>Here </h3>
    <div class="agileinfo-subscribe">
      <form action="#" method="post">
        <input type="text" placeholder="Name" name="Name"  required="">
        <input type="email" placeholder="Email" name="Email"  required="">
        <input type="submit" value="Subscribe">
        <div class="clearfix"> </div>
      </form>
    </div>
  </div>
</div>
<!-- //news-letter -->

@endsection
