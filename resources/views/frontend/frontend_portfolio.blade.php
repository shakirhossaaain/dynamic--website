@extends('frontend.layout.frontend_layout')

@section('title', 'Portfolio')
@section('content')

<div style="background:url({{asset('storage').'/'.$Slider_Section_all->home_page_under_slider_picture}}) no-repeat 0px 0px;" class="banner1">
</div>
<!-- portfolio -->
<div class="portfolio">
  <div class="container">
    <div class="agileits-title">
      <h2 class="w3ls_head"><span>Port</span>folio</h2>
    </div>
    <ul class="simplefilter w3layouts agileits">
      <li class="active w3layouts agileits" data-filter="all">All</li>
      <li class="w3layouts agileits" data-filter="1">Category1</li>
      <li class="w3layouts agileits" data-filter="2">Category2</li>
      <li class="w3layouts agileits" data-filter="3">Category3</li>
      <li class="w3layouts agileits" data-filter="4">Category4</li>
      <li class="w3layouts agileits" data-filter="5">Category5</li>
    </ul>

    <div class="filtr-container w3layouts agileits">

      <div class="filtr-item w3layouts agileits portfolio-t" data-category="1, 5" data-sort="Busy streets">
        <a href="images/g1.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g1.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="2, 5" data-sort="Luminous night">
        <a href="images/g2.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g2.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="1, 4" data-sort="City wonders">
        <a href="images/g3.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g3.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="3" data-sort="In production">
        <a href="images/g4.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g4.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="3, 4" data-sort="Industrial site">
        <a href="images/g5.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g5.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="2, 4" data-sort="Peaceful lake">
        <a href="images/g6.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g6.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="1, 5" data-sort="City lights">
        <a href="images/g7.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g7.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="2, 4" data-sort="Dreamhouse">
        <a href="images/g8.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g8.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>

      <div class="filtr-item w3layouts agileits" data-category="2, 4" data-sort="Dreamhouse">
        <a href="images/g1.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
          <figure>
            <img src="images/g1.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
            <figcaption>
              <h3>Negotiation</h3>
            </figcaption>
          </figure>
        </a>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<!-- //portfolio -->

@endsection
