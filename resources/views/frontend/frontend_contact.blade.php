@extends('frontend.layout.frontend_layout')

@section('title', 'Contact')
@section('content')

<div style="background:url({{asset('storage').'/'.$Slider_Section_all->home_page_under_slider_picture}}) no-repeat 0px 0px;" class="banner1">
</div>
<!-- contact -->

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

	<div class="contact agileits">
		<div class="container">
			<div class="agileits-title">
				<h2 class="w3ls_head"><span>Contact </span>Us</h2>
			</div>
			<form action="{{ route('contact') }}" method="post">
				@csrf
			<div class="contact-agileinfo">
				<div class="col-md-7 contact-form wthree">
						<input id="name" type="text" class="form-control{{ $errors->has('Sender_Name') ? ' is-invalid' : '' }}" name="Sender_Name" placeholder="Name" required="">
						@if ($errors->has('Sender_Name'))
	              <span class="invalid-feedback" role="alert">
	                  <strong>{{ $errors->first('Sender_Name') }}</strong>
	              </span>
	          @endif
						<input id="email" class="email form-control{{ $errors->has('Sender_Email') ? ' is-invalid' : '' }}" type="email" name="Sender_Email" placeholder="Email" required="">
						@if ($errors->has('Sender_Email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('Sender_Email') }}</strong>
                </span>
            @endif
						<textarea placeholder="Message" name="Sender_Message" required=""></textarea>
						@if ($errors->has('Sender_Message'))
	              <span class="invalid-feedback" role="alert">
	                  <strong>{{ $errors->first('Sender_Message') }}</strong>
	              </span>
	          @endif
						<input type="submit" value="SUBMIT">
				</div>
			</form>
				<div class="col-md-4 contact-right wthree">
					<div class="contact-text w3-agileits">
						<h4>GET IN TOUCH :</h4>
						<p><i class="fa fa-map-marker"></i> Broome St, NY 10002, Canada. </p>
						<p><i class="fa fa-phone"></i> Telephone : +00 111 222 3333</p>
						<p><i class="fa fa-fax"></i> FAX : +1 888 888 4444</p>
						<p><i class="fa fa-envelope-o"></i> Email : <a href="mailto:example@mail.com">mail@example.com</a></p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //contact -->
<!-- map -->
	<div class="map w3layouts">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.9503398796587!2d-73.9940307!3d40.719109700000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1441710758555"></iframe>
	</div>
<!-- //map -->

@endsection
