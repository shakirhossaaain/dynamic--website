<!DOCTYPE html>
<html lang="en">
<head>
<title> NEGOTIATION :: @yield('title') </title>

@include('frontend.partials.frontend_header')

@include('frontend.partials.frontend_navbar')

@yield('content')

@include('frontend.partials.frontend_footer')


<!-- for bootstrap working -->
<script src="{{asset('main_web_template/js/bootstrap.js')}}"></script>
<!-- //for bootstrap working -->
</body>
</html>
