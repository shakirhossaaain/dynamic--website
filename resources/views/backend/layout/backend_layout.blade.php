<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Controll ::: @yield('title')</title>

@include('backend.partials.backend_header')

@include('backend.partials.backend_navbar')

@yield('content')

@include('backend.partials.backend_footer')

    <script src="{{asset('admin_web_template/js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('admin_web_template/js/popper1.min.js')}}"></script>
    <script src="{{asset('admin_web_template/js/plugins.js')}}"></script>
    <script src="{{asset('admin_web_template/js/main.js')}}"></script>


</body>
</html>
