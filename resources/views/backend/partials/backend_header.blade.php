

<meta name="description" content="Sufee Admin - HTML5 Admin Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="apple-touch-icon" href="apple-icon.png">
<link rel="shortcut icon" href="favicon.ico">

<!-- Styles -->
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/normalize.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/flag-icon.min.less')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/cs-skin-elastic.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/scss/style.css')}}">
<link rel="stylesheet" href="{{asset('admin_web_template/css/lib/vector-map/jqvmap.min.css')}}">

<!-- Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{asset('admin_web_template/js/html5shiv.min.js')}}"></script>

</head>
<body>
