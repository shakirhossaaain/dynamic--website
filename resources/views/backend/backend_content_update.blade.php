@extends('backend.layout.backend_layout')

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<form action="{{route('content_update_Home_Page_Slider')}}" method="post" class="" enctype="multipart/form-data">
  @csrf
    <div class="col-lg-6">
      <div class="card">
        <div class="card-header">
          <strong>Under Navbar Picture</strong>
        </div>
        <div class="card-body card-block">
            <div class="form-group"><label  class=" form-control-label">1st Photo</label><input type="file"  name="1st_Photo"  class="form-control"></div>
            <div class="form-group"><label  class=" form-control-label">Slider 1st Text Header</label><input type="text"  name="Slider_1st_Text_Header"  class="form-control" value="{{ $Slider_Section_all -> Slider_1st_Text_Header}}"></div>
            <div class="form-group"><label  class=" form-control-label">Slider 1st Text</label><input type="text"  name="Slider_1st_Text"  class="form-control" value="{{ $Slider_Section_all -> Slider_1st_Text}}"></div>
            <div class="form-group"><label  class=" form-control-label">Slider 2nd Text Header</label><input type="text"  name="Slider_2nd_Text_Header"  class="form-control" value="{{ $Slider_Section_all ->Slider_2nd_Text_Header }}"></div>
            <div class="form-group"><label  class=" form-control-label">Slider 2nd Text</label><input type="text"  name="Slider_2nd_Text"  class="form-control" value="{{ $Slider_Section_all ->Slider_2nd_Text }}"></div>
            <div class="form-group"><label  class=" form-control-label">Slider 3rd Text Header</label><input type="text"  name="Slider_2nd_Text"  class="form-control" value="{{ $Slider_Section_all ->Slider_3rd_Text_Header }}"></div>
            <div class="form-group"><label  class=" form-control-label">Slider 3rd Text</label><input type="text"  name="Slider_2nd_Text"  class="form-control" value="{{ $Slider_Section_all ->Slider_3rd_Text }}"></div>
        </div>
        <div class="card-footer" style="display: flex;align-items: center;justify-content: center;">
          <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-dot-circle-o"></i> Add Image</button>
        </div>
      </div>
    </div>
</form>






@endsection
