@extends('backend.layout.backend_layout')

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div>
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Bordered Table</strong>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Message</th>
                  <th scope="col">Send At</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($allMessages as $allMessage)
                <tr>
                  <td>{{ $allMessage -> Sender_Name }}</td>
                  <td>{{ $allMessage -> Sender_Email }}</td>
                  <td>{{ $allMessage -> Sender_Message }}</td>
                  <td>{{ $allMessage -> created_at ->diffForHumans() }}</td>
                  <td>
                    @if($allMessage->status == 1)
                    <a style="color:blue" href="{{ url('/Admin/contact_mark_as_read').'/'.$allMessage->id}}">Mark as "Read"</a>
                    @else
                    <a  style="color:green">Already Read</a>
                    @endif
                    |
                    <a style="color:red" href="{{ url('/Admin/contact_delete').'/'.$allMessage->id}}">Delete</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{ $allMessages -> links() }}
        </div>
    </div>
</div>

@endsection
