-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2018 at 05:12 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dynamic--website`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact__massages`
--

CREATE TABLE `contact__massages` (
  `id` int(10) UNSIGNED NOT NULL,
  `Sender_Name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Sender_Email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Sender_Message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact__massages`
--

INSERT INTO `contact__massages` (`id`, `Sender_Name`, `Sender_Email`, `Sender_Message`, `created_at`, `updated_at`, `status`) VALUES
(2, 'siam', 'sdfa@cjdfjg', 'sdjhfl sadflkha kshdaf;lhf kskafhdlhf hasfjhdsfj lashflhf ashflhf', '2018-08-24 14:38:09', '2018-08-24 14:46:34', 2),
(3, 'siam', 'sdfa@cjdfjg', 'ahsf;lk lsldahflk sahfdkjh asjhflasjdh sadjdhflh lashflahs lashfdlh', '2018-08-24 14:47:08', '2018-08-24 14:47:24', 2),
(4, 'siam', 'sdfa@cjdfjg', 'rhhh dsfh hfhdfh dfhsh dhfhs', '2018-08-27 04:55:28', '2018-08-27 04:55:40', 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_08_24_024857_create_contact__massages_table', 2),
(14, '2018_08_31_110536_create_slider__sections_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slider__sections`
--

CREATE TABLE `slider__sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `under_slider_picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slider_1st_Text_Header` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slider_1st_Text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slider_2nd_Text_Header` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slider_2nd_Text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slider_3rd_Text_Header` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Slider_3rd_Text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider__sections`
--

INSERT INTO `slider__sections` (`id`, `under_slider_picture`, `Slider_1st_Text_Header`, `Slider_1st_Text`, `Slider_2nd_Text_Header`, `Slider_2nd_Text`, `Slider_3rd_Text_Header`, `Slider_3rd_Text`, `created_at`, `updated_at`) VALUES
(1, 'website_page_pictures/YYhbtsQ6WvRITrKtZeFz7Rs2y4gWNehEQK3o44hQ.jpeg', 'kFHLHKHhfl', 'amsnfn', 'ajshfdsjfh', 'KFJLHHF', 'kasjfdlkjdsfk', 'askfdfklsjdfklj', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'shakir', 'shakir@gmail.com', '$2y$10$j97GmiNKFdBFB51dEpKsiOXMr40LGFKVn1NyTHTnZtoXItSq4QDxa', '7kIkhhre2PbMXbntHV5FSHA5eDnkqJjJftNdtwu5jz5up3TOLYw61I5dM7Ko', '2018-08-15 22:13:11', '2018-08-15 22:13:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact__massages`
--
ALTER TABLE `contact__massages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `slider__sections`
--
ALTER TABLE `slider__sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact__massages`
--
ALTER TABLE `contact__massages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `slider__sections`
--
ALTER TABLE `slider__sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
