<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

//backend routes
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('backend')->namespace('Backend')->group(function () {
  Route::get('/home', 'HomeController@index')->name('home');

});

//frontend routes
Route::prefix('frontend')->namespace('Frontend')->group(function () {
  Route::get('/home', 'HomeController@index')->name('home');

});
