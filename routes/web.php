<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.frontend_services');
// });

Auth::routes();

// backend routes
Route::prefix('Admin')->namespace('Backend')->group(function () {

  Route::get('/index', 'BackendController@index')->name('index');
  Route::get('/contact_message', 'BackendController@contact_message')->name('contact_message');
  Route::get('/content_update', 'BackendController@content_update')->name('content_update');
  Route::get('/contact_delete/{contact_id}', 'BackendController@contact_delete')->name('contact_delete');
  Route::get('/contact_mark_as_read/{contact_id}', 'BackendController@contact_mark_as_read')->name('contact_mark_as_read');
  Route::post('/content_update_Home_Page_Slider', 'BackendController@content_update_Home_Page_Slider')->name('content_update_Home_Page_Slider');

});

//frontend routes
Route::namespace('Frontend')->group(function () {
  Route::get('/', 'FrontendController@home')->name('home');
});
Route::prefix('web')->namespace('Frontend')->group(function () {
  //Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/services', 'FrontendController@services')->name('services');
  Route::get('/portfolio', 'FrontendController@portfolio')->name('portfolio');
  Route::get('/contact', 'FrontendController@contact')->name('contact');
  Route::post('/contact', 'FrontendController@contact_post')->name('contact');

});
