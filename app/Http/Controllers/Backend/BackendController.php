<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact_Massage;
use App\Slider_Section;
use Storage;

class BackendController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

      return view('backend.backend_index');
  }

  public function content_update()
  {
      $Slider_Section_all = Slider_Section::find(1);
      return view('backend.backend_content_update', compact('Slider_Section_all'));
  }

  public function contact_message()
  {
      $allMessages = Contact_Massage::paginate(2);
      return view('backend.backend_contact_message',compact('allMessages'));
  }

  public function contact_delete($contact_id)
  {
    Contact_Massage::findOrFail($contact_id)->delete();
    return back()->with('status','Contact Massage Delete  Successfully .');
  }

  public function contact_mark_as_read($contact_id)
  {
    Contact_Massage::findorfail($contact_id)->update([
          'status' => '2'
      ]);
    return back();
  }

  public function content_update_Home_Page_Slider(Request $request)
  {
    $request->validate([
      '1st_Photo' => 'required',
    ]);
    Storage::delete(Slider_Section::find(1)->under_slider_picture);
    $path = $request->file('1st_Photo')->store('website_page_pictures');
    Slider_Section::find(1)->update([
      'under_slider_picture' => $path
    ]);
    return back()->with('status','Home Page Under Slider Picture Updated Successfully .');

  }

}
