<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Contact_Massage;
use App\Slider_Section;

class FrontendController extends Controller
{
    public function home()
    {
      $Slider_Section_all = Slider_Section::find(1);
      return view('frontend.frontend_home',compact('Slider_Section_all'));
    }

    public function services()
    {
      $Slider_Section_all = Slider_Section::find(1);
      return view('frontend.frontend_services',compact('Slider_Section_all'));
    }

    public function portfolio()
    {
      $Slider_Section_all = Slider_Section::find(1);
      return view('frontend.frontend_portfolio',compact('Slider_Section_all'));
    }

    public function contact()
    {
      $Slider_Section_all = Slider_Section::find(1);
      return view('frontend.frontend_contact',compact('Slider_Section_all'));
    }

    public function contact_post(Request $request)
    {
      Contact_Massage::insert([
            'Sender_Name' => $request->Sender_Name,
            'Sender_Email' => $request->Sender_Email,
            'Sender_Message' => $request->Sender_Message,
            'created_at' => Carbon::now()
        ]);
     return back()->with('status','Message Sent  Successfully .');
    }

}
