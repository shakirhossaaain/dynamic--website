<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider_Section extends Model
{
    protected $fillable = ['home_page_under_slider_picture'];
}
